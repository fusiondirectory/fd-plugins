# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Choi Chris <chulwon.choi@gmail.com>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2025-02-11 10:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:51+0000\n"
"Last-Translator: Choi Chris <chulwon.choi@gmail.com>, 2019\n"
"Language-Team: Korean (https://app.transifex.com/fusiondirectory/teams/12202/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: management/debconfProfile/class_debconfProfileManagement.inc:34
#: management/systems/debconf/class_debconfStartup.inc:31
msgid "Debconf"
msgstr "Debconf"

#: management/debconfProfile/class_debconfProfileManagement.inc:35
msgid "Debconf profile management"
msgstr "Debconf 프로필 관리"

#: management/debconfProfile/class_debconfProfileGeneric.inc:68
msgid "There is no template for this profile"
msgstr "이 프로필에 대한 템플릿이 없습니다"

#: management/debconfProfile/class_debconfProfileGeneric.inc:86
#, php-format
msgid "Can't find entry %s in LDAP for profile %s"
msgstr "프로필 %s의 LDAP에서 항목 %s를 찾을 수 없습니다."

#: management/debconfProfile/class_debconfProfileGeneric.inc:159
#, php-format
msgid ""
"In order to import a debconf file, please run the following command : "
"<br/><i>debconf2ldif.pl -b ou=<b>name</b>,%s -k <b>filename</b> > "
"template.ldif </i><br/>With <b>filename</b> the file name, and <b>name</b> "
"the desired name for the template.<br/>"
msgstr ""
"debconf 파일을 임포트하기 위해서는 아래 명령을 실행해 주세요 : <br/><i>debconf2ldif.pl -b "
"ou=<b>name</b>,%s -k <b>filename</b> > template.ldif </i><br/>With "
"<b>filename</b> the file name, and <b>name</b> the desired name for the "
"template.<br/>"

#: management/debconfProfile/class_debconfProfileGeneric.inc:171
#: management/debconfProfile/class_debconfProfileGeneric.inc:176
msgid "Debconf profile"
msgstr "Debconfi 프로필"

#: management/debconfProfile/class_debconfProfileGeneric.inc:172
msgid "Debconf profile information"
msgstr "Debconf 프로필 정보"

#: management/debconfProfile/class_debconfProfileGeneric.inc:190
#: management/debconfProfile/class_debconfProfileGeneric.inc:193
msgid "Name"
msgstr "명칭"

#: management/debconfProfile/class_debconfProfileGeneric.inc:192
msgid "Import a debconf file"
msgstr "debconf 파일 임포트"

#: management/debconfProfile/class_debconfProfileGeneric.inc:193
msgid "Name of this debconf template"
msgstr "debconf 템플릿의 이름"

#: management/debconfProfile/class_debconfProfileGeneric.inc:197
msgid "Entries"
msgstr "항목"

#: management/debconfProfile/class_debconfProfileGeneric.inc:199
msgid "Debconf template answers"
msgstr "Debconf 템플릿 응답"

#: management/debconfProfile/class_debconfProfileGeneric.inc:214
msgid "Import"
msgstr "가져오기"

#: management/systems/debconf/class_debconfStartup.inc:32
msgid "Debconf preseed startup"
msgstr "Debconf 미리 시작"

#: management/systems/debconf/class_debconfStartup.inc:46
msgid "Debconf settings"
msgstr "Debconf 설정"

#: management/systems/debconf/class_debconfStartup.inc:49
msgid "Profile"
msgstr "프로필"

#: management/systems/debconf/class_debconfStartup.inc:49
msgid "Debconf preseed profile to be used for installation"
msgstr "설치에 사용되는 Debconf 미리 설정 프로파일"

#: management/systems/debconf/class_debconfStartup.inc:53
msgid "Release"
msgstr "릴리즈"

#: management/systems/debconf/class_debconfStartup.inc:53
msgid "Debian release to install"
msgstr "설치될 Debian 릴리즈"
