# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2025-02-11 10:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:05+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Russian (https://app.transifex.com/fusiondirectory/teams/12202/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: backend/sympa/class_sympaConfig.inc:26
msgid "Sympa configuration"
msgstr ""

#: backend/sympa/class_sympaConfig.inc:27
msgid "FusionDirectory sympa plugin configuration"
msgstr ""

#: backend/sympa/class_sympaConfig.inc:40
#: management/sympa/class_sympaManagement.inc:28
msgid "Sympa"
msgstr ""

#: backend/sympa/class_sympaConfig.inc:43
msgid "Sympa RDN"
msgstr ""

#: backend/sympa/class_sympaConfig.inc:43
msgid "Branch in which Sympa objects will be stored"
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:27
#: management/systems/services/sympa/class_serviceSympa.inc:28
msgid "Sympa Server"
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:28
msgid "Services"
msgstr "Сервисы"

#: management/systems/services/sympa/class_serviceSympa.inc:43
#: management/sympa/class_sympaAlias.inc:61
msgid "Sympa server"
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:46
msgid "URL"
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:46
msgid "URL to access the sympa server"
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:50
msgid "User"
msgstr "Пользователь"

#: management/systems/services/sympa/class_serviceSympa.inc:50
msgid "User to access sympa server SOAP API."
msgstr ""

#: management/systems/services/sympa/class_serviceSympa.inc:54
msgid "Password"
msgstr "Пароль"

#: management/systems/services/sympa/class_serviceSympa.inc:54
msgid "Password to access sympa server SOAP API."
msgstr ""

#: management/sympa/class_sympaManagement.inc:29
msgid "Sympa management"
msgstr ""

#: management/sympa/class_sympaManagement.inc:30
msgid "Manage sympa aliases"
msgstr ""

#: management/sympa/class_sympaAlias.inc:26
#: management/sympa/class_sympaAlias.inc:27
#: management/sympa/class_sympaAlias.inc:30
#: management/sympa/class_sympaAlias.inc:46
msgid "Sympa list alias"
msgstr ""

#: management/sympa/class_sympaAlias.inc:49
msgid "Name"
msgstr "Фамилия"

#: management/sympa/class_sympaAlias.inc:49
msgid "Name to identify this alias"
msgstr "Имя определяющее этот псеводоним"

#: management/sympa/class_sympaAlias.inc:51
msgid "Description"
msgstr "Описание"

#: management/sympa/class_sympaAlias.inc:51
msgid "Description of this alias"
msgstr "Описание псеводонима"

#: management/sympa/class_sympaAlias.inc:56
msgid "Email address"
msgstr "Адрес электронной почты"

#: management/sympa/class_sympaAlias.inc:61
msgid "Sympa server for this alias"
msgstr ""
