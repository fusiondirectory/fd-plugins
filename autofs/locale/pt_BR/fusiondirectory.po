# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2025-02-11 10:47+0000\n"
"PO-Revision-Date: 2018-08-13 19:50+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Portuguese (Brazil) (https://app.transifex.com/fusiondirectory/teams/12202/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: backend/autofs/class_autofsConfig.inc:26
msgid "Autofs configuration"
msgstr "Configuração Autofs"

#: backend/autofs/class_autofsConfig.inc:27
msgid "FusionDirectory autofs plugin configuration"
msgstr "Configuração de plugin autofs FusionDirectory"

#: backend/autofs/class_autofsConfig.inc:40
msgid "AutoFS"
msgstr ""

#: backend/autofs/class_autofsConfig.inc:43
msgid "AutoFS RDN"
msgstr "RDN AutoFS"

#: backend/autofs/class_autofsConfig.inc:43
msgid "Branch in which automount info will be stored"
msgstr "Filial em que informações de montagem automática serão armazenados"

#: management/autofs/class_autofsManagement.inc:36
msgid "Auto fs"
msgstr "Fs automático"

#: management/autofs/class_autofsManagement.inc:37
msgid "Auto fs management"
msgstr "Gerenciados automatico de fs"

#: management/autofs/class_autofsManagement.inc:38
msgid "Manage auto fs mount points and directories"
msgstr ""

#: management/autofs/class_nisObject.inc:27
#: management/autofs/class_nisObject.inc:28
#: management/autofs/class_nisObject.inc:31
#: management/autofs/class_nisObject.inc:51
msgid "Directory"
msgstr "Diretório"

#: management/autofs/class_nisObject.inc:55
#: management/autofs/class_nisMap.inc:50
msgid "Name"
msgstr "Nome"

#: management/autofs/class_nisObject.inc:55
msgid "Name of this directory"
msgstr "Nome deste diretório"

#: management/autofs/class_nisObject.inc:61
msgid "Automount entry"
msgstr "Entrada de automato"

#: management/autofs/class_nisObject.inc:61
msgid ""
"The entry of this directory for the automount daemon.\n"
" For instance 'auto.u' or '-fstype=nfs domaine.tld:/mount/directory'"
msgstr ""
"A entrada deste diretório para o daemon de montagem automática.\n"
"Para a instancia 'auto.u' ou '-fstype=nfs domaine.tld:/mount/directory'"

#: management/autofs/class_nisObject.inc:65
#: management/autofs/class_nisMap.inc:27 management/autofs/class_nisMap.inc:31
#: management/autofs/class_nisMap.inc:46
msgid "Mount point"
msgstr "Ponto de montagem"

#: management/autofs/class_nisObject.inc:65
msgid "The mount point this directory will be placed in"
msgstr "O ponto de montagem deste diretório será em"

#: management/autofs/class_nisMap.inc:28
msgid "Autofs mount point"
msgstr "Ponto de montagem Autofs"

#: management/autofs/class_nisMap.inc:50
msgid "Name of the mount point"
msgstr "Nome do ponto de montagem"
