# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2025-02-11 10:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:06+0000\n"
"Last-Translator: Benoit Mortier <benoit.mortier@fusiondirectory.org>, 2018\n"
"Language-Team: French (https://app.transifex.com/fusiondirectory/teams/12202/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: management/systems/weblink/class_webLink.inc:30
msgid "Web link"
msgstr "Lien Web"

#: management/systems/weblink/class_webLink.inc:31
msgid "Edit web link"
msgstr "Editer le lien web"

#: management/systems/weblink/class_webLink.inc:45
#: management/systems/weblink/class_webLink.inc:48
msgid "Links"
msgstr "Liens"

#: management/systems/weblink/class_webLink.inc:48
msgid "Web links to this computer"
msgstr "Liens Internet de cet ordinateur"

#: management/systems/weblink/class_webLink.inc:54
msgid "Settings"
msgstr "Paramètres"

#: management/systems/weblink/class_webLink.inc:57
msgid "Protocol"
msgstr "Protocole"

#: management/systems/weblink/class_webLink.inc:57
msgid "Protocol to use to access this computer Web page"
msgstr "Protocole à utiliser pour accéder à cette page Web"

#: management/systems/weblink/class_webLink.inc:63
msgid "Arbitrary links"
msgstr "Liens arbitraires"

#: management/systems/weblink/class_webLink.inc:63
msgid "Any URL you want to associate to this computer"
msgstr "Toute URL que vous souhaitez associer à cet ordinateur"
