# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2025-02-11 10:47+0000\n"
"PO-Revision-Date: 2018-08-13 20:03+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Spanish (https://app.transifex.com/fusiondirectory/teams/12202/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:28
#: management/systems/services/spam/class_serviceSpamAssassin.inc:29
msgid "Spamassassin service"
msgstr ""

#: management/systems/services/spam/class_serviceSpamAssassin.inc:29
msgid "Services"
msgstr "Servicios"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:44
msgid "Spam tagging"
msgstr ""

#: management/systems/services/spam/class_serviceSpamAssassin.inc:47
msgid "Rewrite header"
msgstr "Reescribir cabecera"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:51
msgid "Required score"
msgstr "Puntuación mínima"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:51
msgid "Select required score to tag mail as spam"
msgstr "Indique la puntuación necesaria para marcar el correo como spam"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:57
msgid "Trusted networks"
msgstr "Redes de confianza"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:64
msgid "Flags"
msgstr ""

#: management/systems/services/spam/class_serviceSpamAssassin.inc:67
msgid "Enable use of bayes filtering"
msgstr "Activar el uso de filtros bayesianos"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:72
msgid "Enable bayes auto learning"
msgstr "Activar aprendizaje bayesiano"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:77
msgid "Enable RBL checks"
msgstr "Activas comprobaciones RBL"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:82
msgid "Enable use of Razor"
msgstr "Activar usar Razor"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:87
msgid "Enable use of DCC"
msgstr ""

#: management/systems/services/spam/class_serviceSpamAssassin.inc:92
msgid "Enable use of Pyzor"
msgstr "Activar uso de Pyzor"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:107
msgid "Rules"
msgstr "Reglas"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:111
#: management/systems/services/spam/class_serviceSpamAssassin.inc:119
msgid "Edit spam rule"
msgstr ""

#: management/systems/services/spam/class_serviceSpamAssassin.inc:114
msgid "Name"
msgstr "Nombre"

#: management/systems/services/spam/class_serviceSpamAssassin.inc:115
msgid "Rule"
msgstr "Papel desempeñado"

#: personal/spamassassin/class_spamAssassinAccount.inc:28
msgid "SpamAssassin"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:29
msgid "Edit user's spam rules"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:45
msgid "Spam rules"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:52
msgid "SpamAssassin user preferences settings"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:56
msgid "Directive"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:56
msgid "The name of a SpamAssassin configuration directive"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:61
msgid "Value"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:61
msgid "The value for the directive"
msgstr ""

#: personal/spamassassin/class_spamAssassinAccount.inc:68
msgid "SpamAssassin user rules"
msgstr ""
